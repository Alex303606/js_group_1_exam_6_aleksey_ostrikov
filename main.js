$(function () {
    $('#followUser').on('click', function () {
        $('body').append('<div id="wrap"></div>');
        $('#followUserModal').show();
        $('#followUserModal form').trigger('reset');
        $('#email').attr('placeholder', 'First name:').removeClass('error');
    });
    
    $(document).on('click', '#wrap', function () {
        $(this).remove();
        $('.modal').hide();
    });
    
    $('#editProfile').on('click', function () {
        $('body').append('<div id="wrap"></div>');
        $('#editProfileModal').show();
        $('#editProfileModal form').trigger('reset');
        $('#name').attr('placeholder', 'First name:').removeClass('error');
        $('#lastName').attr('placeholder', 'Last name:').removeClass('error');
    });
    
    var myUrl = 'http://146.185.154.90:8000/blog/alex303606@gmail.com';
    
    async function getProfile() {
        try {
            var profile = await $.get(myUrl + '/profile');
            $('#myName').text(profile.firstName + ' ' + profile.lastName);
        } catch (error) {
            alert('Ошибка: ' + error.status);
        }
    }
    
    getProfile();
    
    $('#editSave').on('click', async function (e) {
        e.preventDefault();
        var editedName = $('#name').val();
        var editedLastName = $('#lastName').val();
        if (editedName !== '' && editedLastName !== '') {
            try {
                await $.ajax({
                    method: 'POST',
                    url: myUrl + '/profile',
                    data: {
                        firstName: editedName,
                        lastName: editedLastName
                    }
                });
                $('#wrap').remove();
                $('.modal').hide();
                getProfile();
                $('.message-list .message').remove();
                getPosts();
            } catch (error) {
                alert('Ошибка: ' + error.status);
            }
        } else {
            $('#name').attr('placeholder', 'Поле не дожно быть пустым!!!').addClass('error');
            $('#lastName').attr('placeholder', 'Поле не дожно быть пустым!!!').addClass('error');
        }
    });
    
    var time = '';
    
    async function getPosts() {
        try {
            var posts = await $.get(myUrl + '/posts');
            if (posts.length !== 0) {
                posts.map(function (post) {
                    $('.message-list').prepend(`<div class="message">
                    <div class="name">` + post.user.firstName + ` ` + post.user.lastName + `
                        <span class="time">` + moment(post.datetime).format('DD MMM YYYY, HH:mm') + `</span>
                    </div>
                    <div class="text">` + post.message + `</div>
                </div>`);
                    time = post.datetime;
                });
            }
        } catch (error) {
            alert('Ошибка: ' + error.status);
        }
    }
    
    setInterval(async function () {
        try {
            var lastMessage = await $.get(myUrl + '/posts?datetime=' + time);
            lastMessage.map(function (message) {
                $('.message-list').prepend(`<div class="message">
                <div class="name">` + message.user.firstName + ` ` + message.user.lastName + `
                    <span class="time">` + moment(message.datetime).format('DD MMM YYYY, HH:mm') + `</span>
                </div>
                <div class="text">` + message.message + `</div>
            </div>`);
                time = message.datetime;
            });
        } catch (error) {
            alert('Ошибка: ' + error.status);
        }
    }, 2000);
    
    getPosts();
    
    async function makePost(message) {
        try {
            await $.ajax({
                method: 'POST',
                url: myUrl + '/posts',
                data: {
                    message: message
                }
            });
        } catch (error) {
            alert('Ошибка: ' + error.status);
        }
    }
    
    $('#send').on('click', function (e) {
        e.preventDefault();
        var messageText = $('#messageText').val();
        if (messageText !== '') {
            makePost(messageText);
            $(this).closest('form').trigger('reset');
            $('#messageText').attr('placeholder', 'Enter your message:').removeClass('error');
        } else {
            $('#messageText').attr('placeholder', 'Поле не дожно быть пустым!!!').addClass('error');
        }
    });
    
    $('#addUser').on('click', async function (e) {
        e.preventDefault();
        var followEmail = $('#email').val();
        if (followEmail !== '') {
            var follow = await $.ajax({
                method: 'POST',
                url: myUrl + '/subscribe',
                data: {
                    email: followEmail
                }
            });
            if (follow.error) {
                alert(follow.error);
            } else {
                $('#wrap').remove();
                $('.modal').hide();
            }
            getPosts();
        } else {
            $('#email').attr('placeholder', 'Поле не дожно быть пустым!!!').addClass('error');
        }
    });
    
    $('#removeAll').on('click', function () {
        try {
            $.post(myUrl + '/subscribe/delete');
            $('.message-list .message').remove();
            getPosts();
        } catch (error) {
            alert('Ошибка: ' + error.status);
        }
    });
    
    $('#followers').on('click', async function () {
        var subscribes = await $.get(myUrl + '/subscribe');
        if (subscribes.error) {
            alert(subscribes.error);
        } else if (subscribes.length !== 0) {
            $('.userListItem').remove();
            subscribes.map(function (user) {
                $('#subscribes').append('<li class="userListItem">' + user.email + '</li>')
            });
            $('#subscribes').show();
            $('body').append('<div id="wrap"></div>');
        } else {
            alert('Нет подписчиков!');
        }
    });
});